$('#btn-submit-bar').click(function() {
    google.load("visualization", "1", {packages:["bar"]});
    var pts ;
    var mapBranchName = {
        'CSE':'COMPUTER SCIENCE & ENGINEERING',
        'IT': 'INFORMATION TECHNOLOGY',
        'EIE': 'INSTRUMENTATION & ELECTRICAL ENGINEERING',
        'ECE': 'ELECTRONICS & COMMUNICATION ENGINEERING',
        'EEE': 'ELECTRICAL & ELECTRONICS ENGINEERING',
        'ME': 'MECHANICAL ENGINEERING',
        'CE': 'CIVIL ENGINEERING',
        'EE': 'ELECTRICAL ENGINEERING',
        'MCA': 'MASTER IN COMPUTER APPLICATION',
        'MBA': 'MASTER OF BUSINESS ADMINISTRATION'
    };

    var reverseMapBranchName = {
        'COMPUTER SCIENCE & ENGINEERING' : 'CSE',
        'INFORMATION TECHNOLOGY' : 'IT',
        'INSTRUMENTATION & ELECTRICAL ENGINEERING' : 'EIE',
        'ELECTRICAL & INSTRUMENTATION ENGINEERING' : 'EIE',
        'ELECTRONICS & COMMUNICATION ENGINEERING' : 'ECE',
        'ELECTRICAL & ELECTRONICS ENGINEERING' : 'EEE',
        'MECHANICAL ENGINEERING' : 'ME',
        'CIVIL ENGINEERING' : 'CE',
        'ELECTRICAL ENGINEERING' : 'EE',
        'MASTER IN COMPUTER APPLICATION':'MCA',
        'MASTER OF BUSINESS ADMINISTRATION': 'MBA'
    }

    var mapSemester = {
        '1' : '1st',
        '2' : '2nd',
        '3' : '3rd',
        '4' : '4th',
        '5' : '5th',
        '6' : '6th',
        '7' : '7th',
        '8' : '8th'
    };

    var college = $('#college-bar').val().toLowerCase();
    var degree = $('#degree-bar').val().replace('.','').toLowerCase();
    var batch = $('#batch-bar').val();
    var semester = $('#semester-bar').val()[0];
    /**/
    var subjectCode, url, title;

    url = window.location.origin + '/api/scoreanalysis?' +'college='+college+
        '&degree=' + degree;
    url+= '&batch=' + batch;
    url+= '&semester=' + semester;
    $.ajax({
        type: "GET",
        cache:false,
        url: url,
        beforeSend: function(){
            $('#barmessage').empty();
            $('#barchart').empty();
            $('#loaderDiv').show();
        },
        success: function(doc)
        {
            $("#loaderDiv").hide();
            pts = doc.response.data;
            var row = [];
            var rows = [];
            if(typeof pts === 'undefined') {
                $('#barmessage').append('<h3>Sorry result for this semester is not yet available!</h3>');
            }
            if(typeof pts !== 'undefined') {
                var labels = Object.keys(pts[0]);
                for(pt = 0; pt< labels.length-1; pt++) {
                    row.push(labels[pt]);
                }
                rows.push(row);
                row = [];
                var tot = pts[pt].length;
                for(pt = 0; pt< pts.length-4; pt++) {
                    var o = pts[pt];
                    var labels = Object.keys(o);
                    for (l = 0; l < labels.length - 1; l++) {
                        row.push(o[labels[l]]);
                        if (row.length === labels.length - 1) {
                            rows.push(row);
                            row = [];
                        }
                    }
                    if (rows.length === pts.length - 3) {
                        var data = new google.visualization.arrayToDataTable(rows);
                        var title = batch + ' Batch Performance in ' + mapSemester[semester] + ' Semester';
                        var options = {
                            chart: {
                                title: title
                            },
                            bars: 'vertical',
                            legend: 'left'
                        };

                        var chart = new google.charts.Bar(document.getElementById('barchart'));
                        chart.draw(data, google.charts.Bar.convertOptions(options));
                    }
                }
            }
        }
    });
    function drawChart() {
        var data = new google.visualization.arrayToDataTable(rows);
        var title = batch + ' Batch Performance in ' + mapSemester[semester] + ' Semester';
        var options = {
            chart: {
                title: title
            },
            bars: 'vertical',
            legend: 'left'
        };

        var chart = new google.charts.Bar(document.getElementById('collegecomparison'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
    }
    //});
});

