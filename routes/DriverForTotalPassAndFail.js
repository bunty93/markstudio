function modelForXLS(numberOfSubjectsFailed, lastLabel) {
    var totalStrength = numberOfSubjectsFailed['Total Pass'] + numberOfSubjectsFailed['Total Fail'];
    var requiredXLS = [];
    var keys = Object.keys(numberOfSubjectsFailed);
    for(start = 0; start < keys.length; start++) {
        var objectForXLS = {};
        objectForXLS['label'] = keys[start];
        objectForXLS[lastLabel] = numberOfSubjectsFailed[keys[start]];
        objectForXLS['%'] = (numberOfSubjectsFailed[keys[start]]/totalStrength * 100).toFixed(2);
        requiredXLS.push(objectForXLS);
        if(start === keys.length-1) {
            return requiredXLS;
        }
    }
}
module.exports = {
    modelDataForPassAndFail : function(markListArray, lastLabel) {
        //    var xlsForPassAndFail = {};
        var numberOfSubjectsFailed = {},totalFailCount=0;
        for(start = 0; start < markListArray.length ; start++) {
            markListArray[start] = markListArray[start].toObject();
            var failCount = 0;
            allSubject = markListArray[start].allsubject;
            for(ind = 0; ind < allSubject.length ; ind++) {
                if(allSubject[ind].gradeScored === 'F') {
                    failCount++;
                }
            }
            if(failCount !== 0) {
                var key = 'FAIL IN ' + failCount + ' SUBJECT';
                if(typeof numberOfSubjectsFailed[key] === 'undefined')
                    numberOfSubjectsFailed[key] = 0;
                numberOfSubjectsFailed[key]++;
                totalFailCount+=1;
            }
            if(start === markListArray.length-1) {
                numberOfSubjectsFailed['Total Pass'] = markListArray.length - totalFailCount;
                numberOfSubjectsFailed['Total Fail'] = totalFailCount;
                return modelForXLS(numberOfSubjectsFailed, lastLabel);
            }
        }
    }
};
