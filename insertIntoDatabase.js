/**
 * Created by devil on 24/12/14.
 */
var mongoose = require('mongoose');
var pointersList = [];
var db = mongoose.connection;
var Schema = mongoose.Schema;
var resultsSchema = new Schema({
    name: String,
    registrationNo: Number,
    sgpa: Number
});
var Result = mongoose.model('demo1', resultsSchema);

/*
 * Sort the pointersList with respect to the priority value set.(Nine,Eight,...)
 */
function func(a,b) {
    if(a.priority > b.priority) {
        return 1;
    }
    else {
        return -1;
    }
}

module.exports = {

    /*
     * Stores the values into pointersList.
     */
    setCountData: function (err, count, i,priority) {
        if (err) throw err;
        pointersList.push({
            'whichPt': i,
            'count': count,
            'priority':priority
        });
    },

    /*
     * Counts the no. of students in different ranges.
     */
    countData: function () {
        Result.count({"sgpa": {$lte: 10, $gte: 9 }}, function (err, count) {
            module.exports.setCountData(err, count, 'nine',1);
        });
        Result.count({"sgpa": {$lt: 9, $gte: 8 }}, function (err, count) {
            module.exports.setCountData(err, count, 'eight',2);
        });
        Result.count({"sgpa": {$lt: 8, $gte: 7 }}, function (err, count) {
            module.exports.setCountData(err, count, 'seven',3);
        });
        Result.count({"sgpa": {$lt: 7, $gte: 6 }}, function (err, count) {
            module.exports.setCountData(err, count, 'six',4);
        });
        Result.count({"sgpa": {$lt: 6}}, function (err, count) {
            module.exports.setCountData(err, count, 'rest',5);
        });
    },

    /*
     * Stores the value fetched from BPUT website to mongoDB.
     */
    insertMarkList: function (documentArray) {
        db.on('error', console.error);

        db.once('open', function () {
            var i = 0, cnt = 0;
            if(documentArray.length != 0) {
                for (i = 0; i < documentArray.length; i++) {
                    document = documentArray[i];
                    var bputResult = new Result(document);
                    bputResult.save(function (err, db) {
                        if (err)
                            throw err;

                        cnt++;
                        //console.log(db);
                        if (cnt === documentArray.length) {
                            module.exports.countData();
                        }
                    });
                }
            }
            else {
                module.exports.countData();
            }
        });
        mongoose.connect('mongodb://localhost/test');
    },

    /*
     * Returns the pointersList.
     */
    getCountData: function () {
        pointersList.sort(func);
        return pointersList;
    }

};
