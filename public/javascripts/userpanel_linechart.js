$('#btn-submit-line').click(function() {
    var pts ;
    var mapBranchName = {
        'CSE':'COMPUTER SCIENCE & ENGINEERING',
        'IT': 'INFORMATION TECHNOLOGY',
        'EIE': 'INSTRUMENTATION & ELECTRICAL ENGINEERING',
        'ECE': 'ELECTRONICS & COMMUNICATION ENGINEERING',
        'EEE': 'ELECTRICAL & ELECTRONICS ENGINEERING',
        'ME': 'MECHANICAL ENGINEERING',
        'CE': 'CIVIL ENGINEERING',
        'EE': 'ELECTRICAL ENGINEERING',
        'MCA': 'MASTER IN COMPUTER APPLICATION',
        'MBA': 'MASTER OF BUSINESS ADMINISTRATION'
    };

    var reverseMapBranchName = {
        'COMPUTER SCIENCE & ENGINEERING' : 'CSE',
        'INFORMATION TECHNOLOGY' : 'IT',
        'INSTRUMENTATION & ELECTRICAL ENGINEERING' : 'EIE',
        'ELECTRICAL & INSTRUMENTATION ENGINEERING' : 'EIE',
        'ELECTRONICS & COMMUNICATION ENGINEERING' : 'ECE',
        'ELECTRICAL & ELECTRONICS ENGINEERING' : 'EEE',
        'MECHANICAL ENGINEERING' : 'ME',
        'CIVIL ENGINEERING' : 'CE',
        'ELECTRICAL ENGINEERING' : 'EE',
        'MASTER IN COMPUTER APPLICATION':'MCA',
        'MASTER OF BUSINESS ADMINISTRATION': 'MBA'
    }

    var mapSemester = {
        '1' : '1st',
        '2' : '2nd',
        '3' : '3rd',
        '4' : '4th',
        '5' : '5th',
        '6' : '6th',
        '7' : '7th',
        '8' : '8th'
    };

    var college = $('#college-line').val().toLowerCase();
    var degree = $('#degree-line').val().replace('.','').toLowerCase();
    var batch = $('#batch-line').val();
    var regNo = $('#regNo-line').val();
/**/
    var subjectCode, url, title;
    url = window.location.origin + '/api/studentperformance?' +'college='+college+'&degree=' + degree;
    url+= '&batch=' + batch;
    url+= '&regNo=' + regNo;
    $.ajax({
        type: "GET",
        cache: false,
        url: url,
        beforeSend: function () {
            $('#linechart').empty();
            $('#loaderDivForLine').show();
            $('#linemessage').empty();
        },
        success: function (doc) {
            $("#loaderDivForLine").hide();
            if(doc.status === 'Error') {
                $('#linemessage').append('<h3>Sorry, Either you have entered some wrong data or we don\'t have the data yet!</h3>');
            }
            else {
                pts = doc.response.data;

                var row = [];
                var rows = [];
                if(typeof pts !== 'undefined') {
                    var labels = Object.keys(pts[0]);
                    for (pt = 0; pt < labels.length; pt++) {
                        row.push(labels[pt]);
                    }
                    rows.push(row);
                    row = [];
                    for (pt = 0; pt < pts.length; pt++) {
                        var o = pts[pt];
                        var label = Object.keys(o);
                        row.push(o[label[0]]);
                        row.push(parseFloat(o[label[1]]));
                        rows.push(row);
                        row = [];
                    }
                    var data = new google.visualization.arrayToDataTable(rows);
                    var name = doc.response.name;
                    if(typeof name !== 'undefined')
                        title = name + '\'s Performance';
                    else
                        title = regNo + '\'s Performance';
                    var options = {
                        title: title,
                        pointSize: 5,
                        legend: { position: 'bottom' },
                        vAxis: {
                            viewWindowMode:'explicit',
                            viewWindow: {
                                max:10
                            }
                        }
                    };
                    var chart = new google.visualization.LineChart(document.getElementById('linechart'));
                    /*
                     google.visualization.events.addListener(chart, 'ready', function () {
                     linechart.innerHTML = '<img src="' + chart.getImageURI() + '">';
                     });
                     */
                    chart.draw(data, options);
                }
            }
        }
    });
});

