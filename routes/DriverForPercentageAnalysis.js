function modelForXLS(subjectList, countOfGradesPerSubject) {
    var differentGrades = ['O','E','A','B','C','D','F','S'];
    var requiredXLS = [];
    var totalCountPerSubject = [];
    for(ind = 0; ind < differentGrades.length; ind++) {
        for (start = 0; start < subjectList.length; start++) {
            var subjectCodeWithName = subjectList[start];
            if (typeof totalCountPerSubject[subjectCodeWithName] === 'undefined') {
                totalCountPerSubject[subjectCodeWithName] = 0;
            }
            totalCountPerSubject[subjectCodeWithName] += countOfGradesPerSubject[subjectCodeWithName][differentGrades[ind]];
        }
    }
    for(ind = 0; ind < differentGrades.length; ind++) {
        var objectForXLS = {};
        objectForXLS['Grade'] = differentGrades[ind];
        for(start = 0; start < subjectList.length; start++) {
            var subjectCodeWithName = subjectList[start];
            objectForXLS[subjectCodeWithName] = countOfGradesPerSubject[subjectCodeWithName][differentGrades[ind]];
            objectForXLS[subjectCodeWithName + ' %'] = ((countOfGradesPerSubject[subjectCodeWithName][differentGrades[ind]]/totalCountPerSubject[subjectCodeWithName])*100).toFixed(2);
        }
        requiredXLS.push(objectForXLS);
        if(ind === differentGrades.length -1) {
            objectForXLS = {};
            objectForXLS['Grade'] =  "Total Students";
            for(start = 0; start < subjectList.length; start++) {
                var subjectCodeWithName = subjectList[start];
                objectForXLS[subjectCodeWithName] = totalCountPerSubject[subjectCodeWithName];
                objectForXLS[subjectCodeWithName + ' %'] = 100;
            }
            requiredXLS.push(objectForXLS);
            return requiredXLS;
        }
    }
}

function generateRowForXls(subjectList,markListArray) {
    var countOfGradesPerSubject = [];
    var differentGrades = ['O','E','A','B','C','D','F','S'];
    for(start = 0; start < subjectList.length; start++) {
        var subjectName = subjectList[start];
        if (typeof countOfGradesPerSubject[subjectName] === 'undefined') {
            countOfGradesPerSubject[subjectName] = new Array();
            for (s = 0; s < differentGrades.length; s++) {
                if (typeof countOfGradesPerSubject[subjectName][differentGrades[s]] === 'undefined') {
                    countOfGradesPerSubject[subjectName][differentGrades[s]] = 0;
                }
            }
        }
    }
    for(start = 0; start < markListArray.length ; start++) {
        var allSubject = markListArray[start].allsubject;
        for(ind = 0;  ind < allSubject.length ; ind++) {
            var subjectName = allSubject[ind].subjectCode + ' ' + allSubject[ind].subjectName;
            var gradeScored = allSubject[ind].gradeScored;
            countOfGradesPerSubject[subjectName][gradeScored]+=1;
        }
        if(start === markListArray.length-1) {
            return modelForXLS(subjectList, countOfGradesPerSubject);
        }
    }
}

function getSubjectListWithCodeAndName(markListArray) {
    var subjectExists = {};
    var subjectList = [];

    for (var m = 0; m < markListArray.length; m++) {
        markListArray[m] = markListArray[m].toObject();
        var eachSubject = markListArray[m].allsubject;
        for (var s = 0; s < eachSubject.length; s++) {
            var subjectName = eachSubject[s].subjectCode + ' ' + eachSubject[s].subjectName;
            if (subjectExists[subjectName] !== true) {
                subjectList.push(subjectName);
                subjectExists[subjectName] = true;
            }
        }
        if (m === markListArray.length - 1) {
            return generateRowForXls(subjectList,markListArray);
        }
    }
}

module.exports = {
    modelDataForPercentageAnalysis : function(markListArray) {
        return getSubjectListWithCodeAndName(markListArray);
    }
};

