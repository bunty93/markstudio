/*
 * This function with maxSGPA, minSGPA, averageSGPA, keys(Names of the branches) pushes the max SGPA,
 * min SGPA and Average SGPA row into the requiredXLS array.
 *
 * */

function pushMaxMinAvg(requiredXLS, maxSGPA, minSGPA, averageSGPA, keys, lastLabel) {
    var objectForXLS = {}, collegeMaxSGPA = 0, collegeMinSGPA = 10, collegeAvgSGPA = 0;
    objectForXLS['label'] = 'Maximum Marks';
    for(ind = 0; ind < keys.length; ind++) {
        objectForXLS[keys[ind]] = maxSGPA[keys[ind]];
        collegeMaxSGPA = Math.max(collegeMaxSGPA,maxSGPA[keys[ind]]);
    }
    objectForXLS[lastLabel] = collegeMaxSGPA.toFixed(2);
    requiredXLS.push(objectForXLS);

    objectForXLS = {};
    objectForXLS['label'] = 'Minimum Marks';
    for(ind = 0; ind < keys.length; ind++) {
        objectForXLS[keys[ind]] = minSGPA[keys[ind]];
        collegeMinSGPA = Math.min(collegeMinSGPA,minSGPA[keys[ind]]);
    }
    objectForXLS[lastLabel] = collegeMinSGPA.toFixed(2);
    requiredXLS.push(objectForXLS);

    objectForXLS = {};
    objectForXLS['label'] = 'Average Marks';
    for(ind = 0; ind < keys.length; ind++) {
        objectForXLS[keys[ind]] = averageSGPA[keys[ind]];
        collegeAvgSGPA+= parseInt(averageSGPA[keys[ind]]);
    }
    objectForXLS[lastLabel] = (collegeAvgSGPA/keys.length).toFixed(2);
    requiredXLS.push(objectForXLS);


    return requiredXLS;
}

/*
 * This function receives countPointers and structure them to produce the rows for the excel sheet.
 * And provides the function pushMaxMinAvg with maxSGPA, minSGPA, averageSGPA to push the max SGPA,
 * min SGPA and Average SGPA row into the requiredXLS array.
 *
 * */
function modelForXLS(countPointers, maxSGPA, minSGPA, averageSGPA, noOfStudentsPerBranch, lastLabel) {
    var requiredXLS = [];
    var keys = Object.keys(countPointers);
    var whichPt = Object.keys(countPointers[keys[0]]);
    var totalStudents = 0;
    for(start = 0; start < whichPt.length; start++) {
        var objectForXLS = {};
        objectForXLS['label'] = whichPt[start];
        var totalCount = 0;
        for(ind = 0; ind < keys.length; ind++) {
            if(typeof countPointers[keys[ind]][whichPt[start]] !== 'undefined') {
                totalCount += countPointers[keys[ind]][whichPt[start]];
            }
            else {
                countPointers[keys[ind]][whichPt[start]] = 0;
            }
            objectForXLS[keys[ind]] = countPointers[keys[ind]][whichPt[start]];
        }
        objectForXLS[lastLabel] = totalCount;
        requiredXLS.push(objectForXLS);
        if(start === whichPt.length-1) {
            objectForXLS = {};
            objectForXLS['label'] = 'Total';
            for(ind = 0; ind < keys.length; ind++) {
                objectForXLS[keys[ind]] = noOfStudentsPerBranch[keys[ind]];
                totalStudents+= noOfStudentsPerBranch[keys[ind]];
                averageSGPA[keys[ind]]=(averageSGPA[keys[ind]]/noOfStudentsPerBranch[keys[ind]]).toFixed(2);
            }
            objectForXLS[lastLabel] = totalStudents;
            requiredXLS.push(objectForXLS);
            return pushMaxMinAvg(requiredXLS, maxSGPA, minSGPA, averageSGPA, keys, lastLabel);
        }
    }
}

/*
 * This function structures the JSON for generating an excel sheet which will be of the following type
 * |     label       | Branch Name(s)                    | Batch Name                                        |
 * --------------------------------------------------------------------------------------------------------- |
 * |   Which Point   | No. of students with this point   | Count of students of entire batch with this point |
 * |     Total       | No. of students of this branch    | Total count Of students                           |
 * |  Maximum SGPA   | Maximum SGPA of this branch       | Maximum SGPA of that batch                        |
 * |  Minimum SGPA   | Minimum SGPA of this branch       | Minimum SGPA of that batch                        |
 * |  Average SGPA   | Average SGPA of this branch       | Average SGPA of that batch                        |
 *
 *
 * It gets the JSON of the entire batch as @markListArray.
 * It finds out the max SGPA, min SGPA,Average SGPA and number of students of each branch. These values are
 * stored in maxSGPA, minSGPA, averageSGPA and noOfStudentsPerBranch respectively.
 * These objects are dynamically structured, they will look something like this :
 *       maxSGPA = {
 *                   'COMPUTER SCIENCE & ENGINEERING' : value,
 *                   .
 *                   .
 *                   .
 *       }
 *
 * The variable countPointers stores the count of different pointers of different branches.
 * It also gets dynamically structured, it will look like this:
 *       countPointers = {
 *                           'COMPUTER SCIENCE & ENGINEERING' : {
 *                                                                   '9 & Above' : value,
 *                                                                   'Between 8 & 9': value,
 *                                                                   .
 *                                                                   .
 *                                                                   .
 *                           },
 *                           .
 *                           .
 *       }
 *   These objects can grow irrespective of number of branches that batch will have.
 *
 * */

module.exports = {
    modelDataForScoreAnalysis: function (markListArray, lastLabel) {
        var labels = ['9 & Above', 'Between 8 & 9', 'Between 7 & 8', 'Between 6 & 7', 'Between 5 & 6', 'Below 5'];
        var mapNumberToDigit = {'9': '9 & Above', '8': 'Between 8 & 9', '7': 'Between 7 & 8', '6': 'Between 6 & 7', '5': 'Between 5 & 6' };
        var countPointers = {}, key;
        var maxSGPA = {}, minSGPA = {}, averageSGPA = {}, noOfstudentsPerBranch = {};
        for (start = 0; start < markListArray.length; start++) {
            markListArray[start] = markListArray[start].toObject();
            var sgpa = markListArray[start].sgpa;
            var branch = markListArray[start].branch;
            var whichPt = String(sgpa).charAt(0);
            if(sgpa === '10.00') {
                whichPt = '9';
            }
            if (mapNumberToDigit[whichPt] === undefined) {
                key = 'Below 5';
            }
            else {
                key = mapNumberToDigit[whichPt];
            }
            if (typeof maxSGPA[branch] === 'undefined')
                maxSGPA[branch] = sgpa;
            if (typeof minSGPA[branch] === 'undefined')
                minSGPA[branch] = sgpa;
            if (typeof averageSGPA[branch] === 'undefined')
                averageSGPA[branch] = 0.0;
            if (typeof noOfstudentsPerBranch[branch] === 'undefined')
                noOfstudentsPerBranch[branch] = 0;
            noOfstudentsPerBranch[branch] += 1;
            averageSGPA[branch] = averageSGPA[branch] + parseInt(sgpa);
            if (maxSGPA[branch] < sgpa) {
                maxSGPA[branch] = sgpa;
            }
            if (minSGPA[branch] > sgpa) {
                minSGPA[branch] = sgpa;
            }
            if (typeof countPointers[branch] === 'undefined') {
                countPointers[branch] = new Object();
                for(var s = 0 ; s < labels.length ; s++) {
                    countPointers[branch][labels[s]] = 0;
                }
                if (typeof countPointers[branch][key] === 'undefined') {
                    countPointers[branch][key] = 0;
                }
                countPointers[branch][key] += 1;
            }
            else {
                if (typeof countPointers[branch][key] === 'undefined') {
                    countPointers[branch][key] = 0;
                }
                countPointers[branch][key] += 1;
            }


            if (start === markListArray.length - 1) {
                return modelForXLS(countPointers, maxSGPA, minSGPA, averageSGPA, noOfstudentsPerBranch, lastLabel);
            }
        }
    }
};