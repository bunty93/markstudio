$('#btn-submit-pie').click(function() {
    var pts ;
    var mapBranchName = {
        'CSE':'COMPUTER SCIENCE & ENGINEERING',
        'IT': 'INFORMATION TECHNOLOGY',
        'EIE': 'INSTRUMENTATION & ELECTRICAL ENGINEERING',
        'ECE': 'ELECTRONICS & COMMUNICATION ENGINEERING',
        'EEE': 'ELECTRICAL & ELECTRONICS ENGINEERING',
        'ME': 'MECHANICAL ENGINEERING',
        'CE': 'CIVIL ENGINEERING',
        'EE': 'ELECTRICAL ENGINEERING',
        'MCA': 'MASTER IN COMPUTER APPLICATION',
        'MBA': 'MASTER OF BUSINESS ADMINISTRATION'
    };


    var mapSemester = {
        '1' : '1st',
        '2' : '2nd',
        '3' : '3rd',
        '4' : '4th',
        '5' : '5th',
        '6' : '6th',
        '7' : '7th',
        '8' : '8th'
    };
    var college = $('#college-pie').val().toLowerCase();
    var degree = $('#degree-pie').val().replace('.','').toLowerCase();
    var batch = $('#batch-pie').val();
    var branch = $('#branch-pie').val().toLowerCase();
    var semester = $('#semester-pie').val()[0];
    var subjectCode, url, title;
    if(typeof $('#subject-pie').val() !== 'undefined')
        subjectCode = $('#subject-pie').val().toUpperCase().trim();
    console.log(subjectCode);
    if(subjectCode === '') {
        url = window.location.origin + '/api/pointersdata?' +'college='+college+ '&degree=' + degree;
        url+= '&batch=' + batch;
        url+= '&branch=' + branch;
        url+= '&semester=' + semester;
        title = mapSemester[semester] + ' Semester Results of ' + batch + ' ' + mapBranchName[branch.toUpperCase()];
    }
    else {
        url = window.location.origin + '/api/subjectwisedata?' +'college='+college+'&degree=' + degree;
        url+= '&batch=' + batch;
        url+= '&branch=' + branch;
        url+= '&semester=' + semester;
        url+= '&subject=' + subjectCode;
        title = mapSemester[semester] + ' Semester Results of ' + batch + ' ';
    }
    $.ajax({/**/
        type: "GET",
        cache:false,
        url: url,
        beforeSend: function(){
            $('#piechart').empty();
            $('#loaderDivForPie').show();
        },
        success: function(doc)
        {
            $("#loaderDivForPie").hide();
            pts = doc;
            val = pts.response.data;
            var subjectName;
            if(typeof pts.response.subjectName !== 'undefined') {
                subjectName = pts.response.subjectName;
                title += subjectName + ' of ' + branch.toUpperCase();
            }
            if(subjectName === " ") {
                title = 'Sorry, this subject code is not found!!!';
            }
                var row = [];
                var rows = [];
                row.push('point');
                row.push('value');
                rows.push(row);
                var label = Object.keys(val);
                for(start = 0; start < label.length; start++) {
                    row = [];
                    row.push(label[start]);
                    row.push(val[label[start]]);
                    rows.push(row);
                }
                var data = new google.visualization.arrayToDataTable(rows);
                var options = {
                    title: title,
                    titleTextStyle: {color: 'black', fontName: 'Arial', fontSize: '18', fontWidth: 'normal'}
                };

                var chart = new google.visualization.PieChart(document.getElementById('piechart'));
/*
                google.visualization.events.addListener(chart, 'ready', function () {
                    piechart.innerHTML = '<img src="' + chart.getImageURI() + '">';
                });
*/
                chart.draw(data, options);
        }
    });
});

