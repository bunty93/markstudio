var mongoose = require('mongoose');

var Schema, resultSchema, Result;
var connection = global.connection;
function createSchema(collectionName) {
    Schema = mongoose.Schema;

    connection.models = {};
    connection.modelSchemas = {};
    resultSchema = new Schema({name: String,
        registrationNo: Number,
        collegeName: String,
        branch: String,
        allsubject: [
            {subjectCode: String,
                subjectName: String,
                creditPoints: Number,
                gradeScored: String}
        ]
    });
    Result = connection.model(collectionName, resultSchema);
}

function getData(cb) {
    Result.find({"$or":[{"sgpa":{$gte : "9"}},{"sgpa":"10.00"}]}).sort({'sgpa':-1}).exec(function (err, db_students) {
        if (err)
            console.log(err);
        cb(null, db_students);
    });
}



module.exports = {
    modelForTopperList : function(res,collectionName) {
        var reqdJSON = {};
        createSchema(collectionName);
        getData(function (err, documents) {
            if (err) {
                console.log(err);
            }
            var markListArray = documents;
            if (markListArray.length === 0) {
                reqdJSON['status'] = 'Error';
                reqdJSON['response'] = {};
                res.send(reqdJSON);
            }
            else {
                reqdJSON['status'] = 'OK';
                reqdJSON['response'] = new Object();
                var reqArray = [];
                for(start = 0; start < markListArray.length ; start++) {
                    markListArray[start] = markListArray[start].toObject();
                    var reqObj = {};
                    reqObj['name'] = markListArray[start].name;
                    reqObj['sgpa'] = markListArray[start].sgpa;
                    reqObj['branch'] = markListArray[start].branch;
                    reqArray.push(reqObj);
                    if(start === markListArray.length-1) {
                        reqdJSON['response']['data'] = reqArray;
                        res.send(reqdJSON);
                    }
                }
            }
        });
    }
};