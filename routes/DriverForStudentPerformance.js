var mongoose = require('mongoose');
var connectionToDB = require('./connection');

var Schema, resultSchema, Result;
var connection = global.connection;
function createSchema(collectionName) {
    Schema = mongoose.Schema;

    connection.models = {};
    connection.modelSchemas = {};
    resultSchema = new Schema({name: String,
        registrationNo: Number,
        collegeName: String,
        branch: String,
        allsubject: [
            {subjectCode: String,
                subjectName: String,
                creditPoints: Number,
                gradeScored: String}
        ]
    });
    Result = connection.model(collectionName, resultSchema);
}

function incSemester(a, b) {

    if (a.Semester > b.Semester) {
        return 1;
    }
    else {
        return -1;
    }
}


function getData(registrationNo, sem, cb) {
    Result.find({'registrationNo': registrationNo}, function (err, db_students) {
        if (err)
            console.log(err);
        cb(null, db_students, sem);
    });
}

function removeDuplicates(reqdJSON) {
    var outputList = [];
    var standardsList = reqdJSON['response']['data'];
    var isAvailable = {};
    for(obj in standardsList) {
        ob = standardsList[obj];
        if(typeof ob.Semester !== 'undefined')
        isAvailable[ob.Semester] = ob.Sgpa;
    }
    var keys = Object.keys(isAvailable);
    for(var key = 0; key < keys.length; key++) {
        outputList.push({
            'Semester' : keys[key],
            'Sgpa' : isAvailable[keys[key]]
        });
    }
    outputList.sort(incSemester);
    reqdJSON['response']['data'] = outputList;
    return reqdJSON;
}


module.exports = {
    modelDataForStudentPerformance: function (res, college, degree, batch, registrationNo, storeData) {
        var sgpaList = [], eachSem = [], reqdJSON = {};
        for (var sem = 1; sem <= 8; sem++) {
            var collectionName = college + degree + batch + 'sem' + sem.toString();
            createSchema(collectionName);
            getData(registrationNo, sem, function (err, documents, semester) {
                if (err) {
                    console.log(err);
                }
                var name;
                var mark = {};
                if(typeof documents !== 'undefined' && typeof documents[0] !== 'undefined') {
                    mark = documents[0].toObject();
                    name = mark.name;
                    if(typeof reqdJSON['response'] === 'undefined') {
                        reqdJSON['response'] = new Object();
                        reqdJSON['response']['name'] = name;
                    }
                    var whichSem = global.mapSemester[semester.toString()] + ' Semester';
                    eachSem = {};
                    eachSem = { 'Semester' : whichSem,
                        'Sgpa' : mark.sgpa
                    }
                }
                sgpaList.push(eachSem);
                if(sgpaList.length === 8) {
                    try {
                        reqdJSON['status'] = 'OK';
                        if(typeof reqdJSON['response']['data'] === 'undefined') {
                            reqdJSON['response']['data'] = new Array();
                        }
                        reqdJSON['response']['data'] = sgpaList;
                        res.send(removeDuplicates(reqdJSON,res));
                    }
                    catch (e) {
                        reqdJSON['status'] = 'Error';
                        reqdJSON['response'] = {};
                        res.send(reqdJSON);
                    }
                }
            });
        }
    }
};