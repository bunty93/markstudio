function modelForXls(resultListOfEachSubject,subjectList) {

    var requiredXLS = [],cnt=0;
    while(true) {
        var objectForXLS = {};
        for(ind = 0; ind < subjectList.length; ind++) {
            var subjectName = subjectList[ind];
            objectForXLS[subjectName] = ' ';
        }
        var flag = 0;
        for(ind = 0; ind < subjectList.length; ind++) {
            var subjectName = subjectList[ind];
            var len = resultListOfEachSubject[subjectName].length;
            if (cnt < len) {
                flag = 1;
                objectForXLS[subjectName] = resultListOfEachSubject[subjectName][cnt];
            }
        }
        if(flag === 0)
            break;
        requiredXLS.push(objectForXLS);
        cnt++;
    }
    return requiredXLS;
}

function generateRowForXls(subjectList,markListArray) {

    var resultListOfEachSubject = [];
    for(start = 0; start < markListArray.length; start++) {
        var allSubject = markListArray[start].allsubject;
        for(subject = 0; subject < allSubject.length ; subject++) {
            var subjectCode = allSubject[subject].subjectCode + ' ' + allSubject[subject].subjectName;
            if(typeof resultListOfEachSubject[subjectCode] === 'undefined') {
                resultListOfEachSubject[subjectCode] = new Array();
            }
            resultListOfEachSubject[subjectCode].push(allSubject[subject].gradeScored);
        }
        if(start === markListArray.length-1) {
            return modelForXls(resultListOfEachSubject,subjectList);
        }
    }

}


/*
 * Get the list of subject by traversing the array and store it in a object.
 *
 * */

function getSubjectListWithCodeAndName(markListArray) {

    var subjectExists = {};
    var subjectList = [];

    for (var m = 0; m < markListArray.length; m++) {
        markListArray[m] = markListArray[m].toObject();
        var eachSubject = markListArray[m].allsubject;
        for (var s = 0; s < eachSubject.length; s++) {
            var subjectName = eachSubject[s].subjectCode + ' ' + eachSubject[s].subjectName;
            if (subjectExists[subjectName] !== true) {
                subjectList.push(subjectName);
                subjectExists[subjectName] = true;
            }
        }
        if (m === markListArray.length - 1) {
            return generateRowForXls(subjectList,markListArray);
        }
    }

}

module.exports = {
    modelDataForSubjectWise : function(markListArray) {
        return getSubjectListWithCodeAndName(markListArray);
    }
};