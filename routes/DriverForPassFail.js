function modelForXLS(subjectList, mapSubjectCodeToName, totalFail, totalPass) {
    var requiredXLS = [];
    for(var ind = 0; ind < subjectList.length ; ind++) {
        var objectForXLS = {};
        var subjectCode = subjectList[ind];
        objectForXLS['Subject Code'] = subjectCode;
        objectForXLS['Fail'] = totalFail[subjectCode];
        var studentsAppearedThisSubject = totalFail[subjectCode] + totalPass[subjectCode];
        objectForXLS['Total Student'] = studentsAppearedThisSubject;
        objectForXLS['% Fail'] = ((totalFail[subjectCode]/studentsAppearedThisSubject)*100).toFixed(2);
        objectForXLS['Subject Name'] = mapSubjectCodeToName[subjectCode];
        objectForXLS['% Pass'] = ((totalPass[subjectCode]/studentsAppearedThisSubject)*100).toFixed(2);
        requiredXLS.push(objectForXLS);
        if(ind === subjectList.length-1) {
            return requiredXLS;
        }
    }
}

function generateRowForXls(markListArray, subjectList, mapSubjectCodeToName) {
    totalFail = [] , totalPass = [];
    for(start = 0; start < markListArray.length; start++) {
        var allSubject = markListArray[start].allsubject;
        for(subject = 0; subject < allSubject.length ; subject++) {
            var subjectCode = allSubject[subject].subjectCode;
            if(typeof totalPass[subjectCode] === 'undefined') {
                totalPass[subjectCode] = 0;
                totalFail[subjectCode] = 0;
            }
            if(allSubject[subject].gradeScored !== 'F') {
                totalPass[subjectCode]+=1;
            }
            else {
                totalFail[subjectCode]+=1;
            }
        }
        if(start === markListArray.length-1) {
            return modelForXLS(subjectList, mapSubjectCodeToName, totalFail, totalPass);
        }
    }
}

function getSubjectListAndName(markListArray) {

    var subjectExists = {};
    var subjectList = [];
    var mapSubjectCodeToName = {};

    for (var m = 0; m < markListArray.length; m++) {
        markListArray[m] = markListArray[m].toObject();
        var eachSubject = markListArray[m].allsubject;
        for (var s = 0; s < eachSubject.length; s++) {
            var subjectCode = eachSubject[s].subjectCode;
            var subjectName = eachSubject[s].subjectName;
            if (subjectExists[subjectCode] !== true) {
                subjectList.push(subjectCode);
                subjectExists[subjectCode] = true;
                mapSubjectCodeToName[subjectCode] = subjectName;
            }
        }
        if (m === markListArray.length - 1) {
            return generateRowForXls(markListArray, subjectList, mapSubjectCodeToName);
        }
    }
}
module.exports = {
    modelDataForSubjectWise : function (markListArray) {
        return getSubjectListAndName(markListArray);
    }
};
