$('#btn-submit-table').click(function() {
    var pts ;
    var mapBranchName = {
        'CSE':'COMPUTER SCIENCE & ENGINEERING',
        'IT': 'INFORMATION TECHNOLOGY',
        'EIE': 'INSTRUMENTATION & ELECTRICAL ENGINEERING',
        'ECE': 'ELECTRONICS & COMMUNICATION ENGINEERING',
        'EEE': 'ELECTRICAL & ELECTRONICS ENGINEERING',
        'ME': 'MECHANICAL ENGINEERING',
        'CE': 'CIVIL ENGINEERING',
        'EE': 'ELECTRICAL ENGINEERING',
        'MCA': 'MASTER IN COMPUTER APPLICATION',
        'MBA': 'MASTER OF BUSINESS ADMINISTRATION'
    };


    var mapSemester = {
        '1' : '1st',
        '2' : '2nd',
        '3' : '3rd',
        '4' : '4th',
        '5' : '5th',
        '6' : '6th',
        '7' : '7th',
        '8' : '8th'
    };
    /*College Created*/
    var college = $('#college-table').val().toLowerCase();
    var degree = $('#degree-table').val().replace('.','').toLowerCase();
    var batch = $('#batch-table').val();
    var semester = $('#semester-table').val()[0];
    var url, title;
    url = window.location.origin + '/api/topperlist?'+'college='+college+'&degree=' + degree;
    url+= '&batch=' + batch;
    url+= '&semester=' + semester;
    title = mapSemester[semester] + ' Semester\'s top performers of ' + batch + ' batch';
    $.ajax({
        type: "GET",
        cache:false,
        url: url,
        beforeSend: function(){
            $('#tablechart').empty();
            $('#tablemessage').empty();
            $('#loaderDivForTable').show();
        },
        success: function(doc)
        {
            $("#loaderDivForTable").hide();
            pts = doc;
            val = pts.response.data;
            var row = [];
            var rows = [];
            row.push('Name');
            row.push('Sgpa');
            row.push('Branch');
            rows.push(row);
            if(typeof val === 'undefined') {
                $('#tablemessage').append('<h3>Sorry, Either there are no 9 pointers or result for this semester is not yet available!</h3>');
            }
            if(typeof val !== 'undefined') {
                for (pt = 0; pt < val.length; pt++) {
                    var o = val[pt];
                    row = [];
                    row.push(o['name']);
                    row.push(parseFloat(o['sgpa']));
                    row.push(o['branch']);
                    rows.push(row);
                }
                var data = new google.visualization.arrayToDataTable(rows);
                var options = {
                    title: title,
                    titleTextStyle: {color: 'black', fontName: 'Arial', fontSize: '12', fontWidth: 'normal'},
                    sort:'enable',
                    sortAscending:false,
                    showRowNumber: true,
                    sortColumn:1,
                    page: 'enable',
                    pageSize : 10
                };
                var chart = new google.visualization.Table(document.getElementById('tablechart'));
                chart.draw(data, options);
            }
        }
    });
});

