/**
 * Created by devil on 17/5/15.
 */

var async = require('async');
var mongoose = require('mongoose');

var Schema, resultSchema, Result;
var connection = global.connection;

function getResultsOfDifferentColleges(college,cb) {

    async.parallel({
            "Above Nine": function (callback) {
                Result.count({"$or": [
                    {"sgpa": {$gte: "9"}},
                    {"sgpa": "10.00"}
                ]}, function (err, count) {
                    if (err)
                        console.log(err);
                    callback(null, count);
                });
            },
            "Between Eight and Nine": function (callback) {
                Result.count({"sgpa": {$gte: "8", $lte: "9" }}, function (err, count) {
                    if (err)
                        console.log(err);
                    callback(null, count);
                });
            },
            "Between Seven and Eight": function (callback) {
                Result.count({"sgpa": {$gte: "7", $lte: "8"  }}, function (err, count) {
                    if (err)
                        console.log(err);
                    callback(null, count);
                });
            },
            "Between Six and Seven": function (callback) {
                Result.count({"sgpa": {$gte: "6", $lte: "7" }}, function (err, count) {
                    if (err)
                        console.log(err);
                    callback(null, count);
                });
            },
            "Below 6": function (callback) {
                Result.count({"sgpa": {$lt: "6" }}, function (err, count) {
                    if (err)
                        console.log(err);
                    callback(null, count);
                });
            }

        },
        function (err, results) {
                cb(null, college, results);
        });
}

function createSchema(colleges, degree, batch, semester, cb) {
    var diffColleges = {},reqCount = 0;
    Schema = mongoose.Schema;
    connection.models = {};
    connection.modelSchemas = {};
    resultSchema = new Schema({name: String,
        registrationNo: Number,
        collegeName: String,
        branch: String,
        allsubject: [
            {subjectCode: String,
                subjectName: String,
                creditPoints: Number,
                gradeScored: String}
        ]
    });
    for (start = 0; start < colleges.length; start++) {
        var college = colleges[start];
        var collectionName = college + degree + batch + semester;
        Result = connection.model(collectionName, resultSchema);
        getResultsOfDifferentColleges(college,function (err, clg, dataDiffColleges) {
            diffColleges[clg] = dataDiffColleges;
            reqCount++;
            if(reqCount === colleges.length) {
                cb(null,diffColleges);
            }
        });
    }
}

module.exports = {
    modelDataForCollegeComparison: function (colleges, degree, batch, semester, cb) {
        createSchema(colleges, degree, batch, semester, function (err, dataDiffColleges) {
            cb(null, dataDiffColleges);
        });
    }
}


