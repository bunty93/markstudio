var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var routes = require('./routes/index');
var excelGenerator = require('./routes/excelgenerator');
var apiGenerator = require('./routes/apigenerator');
var multer = require('multer');
var session = require('express-session');
var json2xls = require('json2xls');
var passport = require('passport');
var flash    = require('connect-flash');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(multer());
app.use(json2xls.middleware);
app.use(flash()); // use connect-flash for flash messages stored in session
app.use(session({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

require('./routes/config/passport')(passport);

app.use(function (req, res, next) {


    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'https://markstudio-nist.rhcloud.com');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

require('./routes/config/passport')(passport);
require('./routes/LoginAndSignup')(app,passport);

app.use('/', routes);
app.use('/',excelGenerator);
app.use('/',apiGenerator);

var server_port = process.env.OPENSHIFT_NODEJS_PORT || 2020;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';


app.listen(server_port,server_ip_address, function () {
    console.log(new Date() + 'Server is listening on port 2020');
});
module.exports = app;