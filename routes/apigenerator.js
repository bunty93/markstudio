var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var driverForPercentageAnalysis = require('./DriverForPercentageAnalysis');
var driverForScoreAnalysis  = require('./DriverForScoreAnalysis');
var driverForTotalPassAndFail = require('./DriverForTotalPassAndFail');
var driverForSubjectWise = require('./DriverForSubjectWise');
var driverForPassFail = require('./DriverForPassFail');
var driverForStudentPerformance = require('./DriverForStudentPerformance');
var driverForTopperList = require('./DriverForTopperList');
var driverForCollegeComparison = require('./DriverForCollegeComparison');
var connection = global.connection;

var Schema, resultSchema, Result;
function createSchema(collectionName) {
    Schema = mongoose.Schema;

    connection.models = {};
    connection.modelSchemas = {};
    resultSchema = new Schema({name: String,
        registrationNo: Number,
        collegeName: String,
        branch: String,
        allsubject: [
            {subjectCode: String,
                subjectName: String,
                creditPoints: Number,
                gradeScored: String}
        ]
    });
    Result = connection.model(collectionName, resultSchema);
}

function getData(cb) {
    Result.find({}, function (err, db_students) {
        if (err)
            console.log(err);
        cb(null, db_students);
    });
}

router.get('/api/pointersdata', function (req, res) {
    var sess = req.session;
    sess.eachList = {};
    sess.reqdJSON = {};
    var college,degree, branch, batch, semester;
    if (typeof (req.query.college) !== 'undefined') {
        college = (req.query.college).toLowerCase();
    }
    else {
        college = '';
    }

    if (typeof (req.query.degree) !== 'undefined') {
        degree = (req.query.degree).toLowerCase();
    }
    else {
        degree = '';
    }
    if (typeof (req.query.branch) !== 'undefined') {
        branch = (req.query.branch).toLowerCase();
    }
    else {
        branch = '';
    }
    if (typeof (req.query.batch) !== 'undefined') {
        batch = req.query.batch;
    }
    else {
        batch = '';
    }

    if (typeof (req.query.semester) !== 'undefined') {
        semester = req.query.semester;
        semester = 'sem' + semester;
    }
    else {
        semester = '';
    }

    var collectionName = college + degree + branch + batch + semester;
    if (collectionName === '') {
        sess.reqdJSON['status'] = 'Error';
        sess.reqdJSON['response'] = {};
        res.send(sess.reqdJSON);
    }
    else {
        createSchema(collectionName);
        console.log(collectionName);
        getData(function (err, documents) {
            if (err) {
                console.log(err);
            }
            var markListArray = documents;
            if(markListArray.length === 0) {
                sess.reqdJSON['status'] = 'Error';
                sess.reqdJSON['response'] = {};
                res.send(sess.reqdJSON);
            }
            else {
                sess.reqdJSON['status'] = 'OK';
                var pts = ['Above Nine' , 'Between Eight and Nine' , 'Between Seven and Eight' , 'Between Six and Seven' , 'Below 6'];
                for (pt = 0; pt < pts.length; pt++) {
                    if (typeof sess.eachList[pts[pt]] === 'undefined') {
                        sess.eachList[pts[pt]] = 0;
                    }
                }
                for (start = 0; start < markListArray.length; start++) {
                    marks = markListArray[start].toObject();
                    if (marks.sgpa[0] === '9' || marks.sgpa === '10.00') {
                        sess.eachList['Above Nine'] += 1;
                    }
                    else if (marks.sgpa[0] === '8') {
                        sess.eachList['Between Eight and Nine'] += 1;
                    }
                    else if (marks.sgpa[0] === '7') {
                        sess.eachList['Between Seven and Eight'] += 1;
                    }
                    else if (marks.sgpa[0] === '6') {
                        sess.eachList['Between Six and Seven'] += 1;
                    }
                    else {
                        sess.eachList['Below 6'] += 1;
                    }
                }
                sess.reqdJSON['response'] = new Object();
                sess.reqdJSON['response']['data'] = sess.eachList;
                sess.reqdJSON['response']['branch'] = global.mapBranchName[branch.toUpperCase()];
                res.send(sess.reqdJSON);
            }
        });
    }
});

router.get('/api/subjectwisedata', function (req, res) {
    var sess = req.session;
    sess.eachList = {};
    sess.reqdJSON = {};
    var college,degree, branch, batch, semester, subjectCode;

    if (typeof (req.query.college) !== 'undefined') {
        college = (req.query.college).toLowerCase();
    }
    else {
        college = '';
    }

    if (typeof (req.query.degree) !== 'undefined') {
        degree = (req.query.degree).toLowerCase();
    }
    else {
        degree = '';
    }
    if (typeof (req.query.branch) !== 'undefined') {
        branch = (req.query.branch).toLowerCase();
    }
    else {
        branch = '';
    }
    if (typeof (req.query.batch) !== 'undefined') {
        batch = req.query.batch;
    }
    else {
        batch = '';
    }
    if (typeof (req.query.subject) !== 'undefined') {
        subjectCode = req.query.subject;
    }
    else {
        subjectCode = '';
    }


    if (typeof (req.query.semester) !== 'undefined') {
        semester = req.query.semester;
        semester = 'sem' + semester;
    }
    else {
        semester = '';
    }

    var collectionName = college + degree + branch + batch + semester;
    if (collectionName === '' || subjectCode === '') {
        sess.reqdJSON['status'] = 'Error';
        sess.reqdJSON['response'] = {};
        res.send(sess.reqdJSON);
    }
    else {
        createSchema(collectionName);
        getData(function (err, documents) {
            if (err) {
                console.log(err);
            }
            var markListArray = documents;
            if(markListArray.length === 0) {
                sess.reqdJSON['status'] = 'Error';
                sess.reqdJSON['response'] = {};
                res.send(sess.reqdJSON);
            }
            else {
                sess.reqdJSON['status'] = 'OK';
                var pts = ['O' , 'E' , 'A' , 'B' , 'C', 'D' , 'F' , 'M' , 'S'];
                for (pt = 0; pt < pts.length; pt++) {
                    if(typeof sess.eachList[pts[pt]] === 'undefined') {
                        sess.eachList[pts[pt]] = 0;
                    }
                }
                var subjectName = ' ';
                for (start = 0; start < markListArray.length; start++) {
                    marks = markListArray[start].toObject();
                    var allSubject = marks.allsubject;
                    for(ind = 0; ind < allSubject.length; ind++) {
                        if(subjectCode === allSubject[ind].subjectCode) {
                            subjectName = allSubject[ind].subjectName;
                            var grade = allSubject[ind].gradeScored;
                            sess.eachList[grade] += 1;
                        }
                    }
                }
                sess.reqdJSON['response'] = new Object();
                sess.reqdJSON['response']['data'] = sess.eachList;
                sess.reqdJSON['response']['subjectName'] = subjectName;
                sess.reqdJSON['response']['branch'] = global.mapBranchName[branch.toUpperCase()];
                res.send(sess.reqdJSON);
            }
        });
    }
});

router.get('/api/scoreanalysis', function(req,res) {
    var sess = req.session;
    sess.reqdJSON = {};
    var college,degree, batch, semester;

    if (typeof (req.query.college) !== 'undefined') {
        college = (req.query.college).toLowerCase();
    }
    else {
        college = '';
    }

    if (typeof (req.query.degree) !== 'undefined') {
        degree = (req.query.degree).toLowerCase();
    }
    else {
        degree = '';
    }
    if (typeof (req.query.batch) !== 'undefined') {
        batch = req.query.batch;
    }
    else {
        batch = '';
    }

    if (typeof (req.query.semester) !== 'undefined') {
        semester = req.query.semester;
        semester = 'sem' + semester;
    }
    else {
        semester = '';
    }

    var collectionName = college + degree + batch + semester;
    var lastLabel = batch + ' batch ' + global.mapSemester[semester[semester.length-1]] + ' Semester';
    if (collectionName === '') {
        sess.reqdJSON['status'] = 'Error';
        sess.reqdJSON['response'] = {};
        res.send(sess.reqdJSON);
    }
    else {
        createSchema(collectionName);
        getData(function (err, documents) {
            if (err) {
                console.log(err);
            }
            var markListArray = documents;
            if (markListArray.length === 0) {
                sess.reqdJSON['status'] = 'Error';
                sess.reqdJSON['response'] = {};
                res.send(sess.reqdJSON);
            }
            else {
                sess.reqdJSON['status'] = 'OK';
                sess.reqdJSON['response'] = new Object();
                sess.reqdJSON['response']['data'] = driverForScoreAnalysis.modelDataForScoreAnalysis(markListArray,lastLabel);
                res.send(sess.reqdJSON);
            }
        });
    }
});

router.get('/api/percentageanalysis', function(req,res) {
    var sess = req.session;
    sess.reqdJSON = {};
    var college,degree, batch, semester;

    if (typeof (req.query.college) !== 'undefined') {
        college = (req.query.college).toLowerCase();
    }
    else {
        college = '';
    }
    if (typeof (req.query.degree) !== 'undefined') {
        degree = (req.query.degree).toLowerCase();
    }
    else {
        degree = '';
    }
    if (typeof (req.query.batch) !== 'undefined') {
        batch = req.query.batch;
    }
    else {
        batch = '';
    }

    if (typeof (req.query.semester) !== 'undefined') {
        semester = req.query.semester;
        semester = 'sem' + semester;
    }
    else {
        semester = '';
    }

    var collectionName = college + degree + batch + semester;
    if (collectionName === '') {
        sess.reqdJSON['status'] = 'Error';
        sess.reqdJSON['response'] = {};
        res.send(sess.reqdJSON);
    }
    else {
        createSchema(collectionName);
        getData(function (err, documents) {
            if (err) {
                console.log(err);
            }
            var markListArray = documents;
            if (markListArray.length === 0) {
                sess.reqdJSON['status'] = 'Error';
                sess.reqdJSON['response'] = {};
                res.send(sess.reqdJSON);
            }
            else {
                sess.reqdJSON['status'] = 'OK';
                sess.reqdJSON['response'] = new Object();
                sess.reqdJSON['response']['data'] = driverForPercentageAnalysis.modelDataForPercentageAnalysis(markListArray);
                res.send(sess.reqdJSON);
            }
        });
    }
});

router.get('/api/totalpassandfail', function(req,res) {
    var sess = req.session;
    sess.reqdJSON = {};
    var college, degree, batch, semester;

    if (typeof (req.query.college) !== 'undefined') {
        college = (req.query.college).toLowerCase();
    }
    else {
        college = '';
    }
    if (typeof (req.query.degree) !== 'undefined') {
        degree = (req.query.degree).toLowerCase();
    }
    else {
        degree = '';
    }
    if (typeof (req.query.batch) !== 'undefined') {
        batch = req.query.batch;
    }
    else {
        batch = '';
    }

    if (typeof (req.query.semester) !== 'undefined') {
        semester = req.query.semester;
        semester = 'sem' + semester;
    }
    else {
        semester = '';
    }

    var collectionName = college + degree + batch + semester;
    var lastLabel = batch + ' batch ' + global.mapSemester[semester[semester.length-1]] + ' Semester';
    if (collectionName === '') {
        sess.reqdJSON['status'] = 'Error';
        sess.reqdJSON['response'] = {};
        res.send(sess.reqdJSON);
    }
    else {
        createSchema(collectionName);
        getData(function (err, documents) {
            if (err) {
                console.log(err);
            }
            var markListArray = documents;
            if (markListArray.length === 0) {
                sess.reqdJSON['status'] = 'Error';
                sess.reqdJSON['response'] = {};
                res.send(sess.reqdJSON);
            }
            else {
                sess.reqdJSON['status'] = 'OK';
                sess.reqdJSON['response'] = new Object();
                sess.reqdJSON['response']['data'] = driverForTotalPassAndFail.modelDataForPassAndFail(markListArray, lastLabel);
                res.send(sess.reqdJSON);
            }
        });
    }
});

router.get('/api/passfailpercentage', function(req,res) {
    var sess = req.session;
    sess.reqdJSON = {};
    var college, degree, batch, semester;

    if (typeof (req.query.college) !== 'undefined') {
        college = (req.query.college).toLowerCase();
    }
    else {
        college = '';
    }
    if (typeof (req.query.degree) !== 'undefined') {
        degree = (req.query.degree).toLowerCase();
    }
    else {
        degree = '';
    }
    if (typeof (req.query.batch) !== 'undefined') {
        batch = req.query.batch;
    }
    else {
        batch = '';
    }

    if (typeof (req.query.semester) !== 'undefined') {
        semester = req.query.semester;
        semester = 'sem' + semester;
    }
    else {
        semester = '';
    }

    var collectionName = college + degree + batch + semester;
    if (collectionName === '') {
        sess.reqdJSON['status'] = 'Error';
        sess.reqdJSON['response'] = {};
        res.send(sess.reqdJSON);
    }
    else {
        createSchema(collectionName);
        getData(function (err, documents) {
            if (err) {
                console.log(err);
            }
            var markListArray = documents;
            if (markListArray.length === 0) {
                sess.reqdJSON['status'] = 'Error';
                sess.reqdJSON['response'] = {};
                res.send(sess.reqdJSON);
            }
            else {
                sess.reqdJSON['status'] = 'OK';
                sess.reqdJSON['response'] = new Object();
                sess.reqdJSON['response']['data'] = driverForPassFail.modelDataForSubjectWise(markListArray);
                res.send(sess.reqdJSON);
            }
        });
    }
});

router.get('/api/subjectwise', function(req,res) {
    var sess = req.session;
    sess.reqdJSON = {};
    var college, degree, batch, semester;
    if (typeof (req.query.college) !== 'undefined') {
        college = (req.query.college).toLowerCase();
    }
    else {
        college = '';
    }
    if (typeof (req.query.degree) !== 'undefined') {
        degree = (req.query.degree).toLowerCase();
    }
    else {
        degree = '';
    }
    if (typeof (req.query.batch) !== 'undefined') {
        batch = req.query.batch;
    }
    else {
        batch = '';
    }

    if (typeof (req.query.semester) !== 'undefined') {
        semester = req.query.semester;
        semester = 'sem' + semester;
    }
    else {
        semester = '';
    }

    var collectionName = college + degree + batch + semester;
    if (collectionName === '') {
        sess.reqdJSON['status'] = 'Error';
        sess.reqdJSON['response'] = {};
        res.send(sess.reqdJSON);
    }
    else {
        createSchema(collectionName);
        getData(function (err, documents) {
            if (err) {
                console.log(err);
            }
            var markListArray = documents;
            if (markListArray.length === 0) {
                sess.reqdJSON['status'] = 'Error';
                sess.reqdJSON['response'] = {};
                res.send(sess.reqdJSON);
            }
            else {
                sess.reqdJSON['status'] = 'OK';
                sess.reqdJSON['response'] = new Object();
                sess.reqdJSON['response']['data'] = driverForSubjectWise.modelDataForSubjectWise(markListArray);
                res.send(sess.reqdJSON);
            }
        });
    }
});

router.get('/api/studentperformance', function(req,res) {
    var sess = req.session;
    sess.reqdJSON = {};
    var college, degree, batch, registrationNo;

    if (typeof (req.query.college) !== 'undefined') {
        college = (req.query.college).toLowerCase();
    }
    else {
        college = '';
    }
    if (typeof (req.query.degree) !== 'undefined') {
        degree = (req.query.degree).toLowerCase();
    }
    else {
        degree = '';
    }
    if (typeof (req.query.batch) !== 'undefined') {
        batch = req.query.batch;
    }
    else {
        batch = '';
    }
    if (typeof (req.query.regNo) !== 'undefined') {
        registrationNo = req.query.regNo;
    }
    else {
        registrationNo = '';
    }
    driverForStudentPerformance.modelDataForStudentPerformance(res, college, degree, batch, registrationNo);
});

/*
router.get('/api/branchwise', function(req,res) {
});
*/

router.get('/api/topperlist', function(req,res) {
    var college, degree, batch, semester;

    if (typeof (req.query.college) !== 'undefined') {
        college = (req.query.college).toLowerCase();
    }
    else {
        college = '';
    }
    if (typeof (req.query.degree) !== 'undefined') {
        degree = (req.query.degree).toLowerCase();
    }
    else {
        degree = '';
    }
    if (typeof (req.query.batch) !== 'undefined') {
        batch = req.query.batch;
    }
    else {
        batch = '';
    }

    if (typeof (req.query.semester) !== 'undefined') {
        semester = req.query.semester;
        semester = 'sem' + semester;
    }
    else {
        semester = '';
    }
    var collectionName = college + degree + batch + semester;
    driverForTopperList.modelForTopperList(res,collectionName);
});

router.get('/api/collegecomparison', function(req,res) {
    var reqdJSON = {};
    var colleges=[], degree, batch,semester;
     if (typeof (req.query.college) !== 'undefined') {
        if(typeof req.query.college === 'string') {
            colleges.push(req.query.college);
        }
         else {
            colleges = req.query.college;
        }
     }
     else {
     colleges = '';
     }

    if (typeof (req.query.degree) !== 'undefined') {
        degree = (req.query.degree).toLowerCase();
    }
    else {
        degree = '';
    }
    if (typeof (req.query.batch) !== 'undefined') {
        batch = req.query.batch;
    }
    else {
        batch = '';
    }
    if (typeof (req.query.semester) !== 'undefined') {
        semester = req.query.semester;
        semester = 'sem' + semester;
    }
    else {
        semester = '';
    }
    driverForCollegeComparison.modelDataForCollegeComparison(colleges,degree,batch,semester,function(err,data) {
        if(err) {
            reqdJSON['status'] = 'Error';
            reqdJSON['response'] = new Object();
        }
        else {
            reqdJSON['status'] = 'OK';
            reqdJSON['response'] = new Object();
            reqdJSON['response']['data'] = data;
            res.send(reqdJSON);
        }
    });
});

router.get('*' , function(req,res) {
   res.render('error');
});

module.exports = router;
