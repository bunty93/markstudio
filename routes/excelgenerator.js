var express = require('express');
var mongoose = require('mongoose');
var async = require('async');
var driverForPercentageAnalysis = require('./DriverForPercentageAnalysis');
var driverForScoreAnalysis = require('./DriverForScoreAnalysis');
var driverForTotalPassAndFail = require('./DriverForTotalPassAndFail');
var driverForSubjectWise = require('./DriverForSubjectWise');
var driverForPassFail = require('./DriverForPassFail');
var connectionToDB = require('./connection');
var connection = global.connection;
var router = express.Router();

var Schema, resultSchema, Result;
function createSchema(collectionName) {
    Schema = mongoose.Schema;

    connection.models = {};
    connection.modelSchemas = {};
    resultSchema = new Schema({name: String,
        registrationNo: Number,
        collegeName: String,
        branch: String,
        allsubject: [
            {subjectCode: String,
                subjectName: String,
                creditPoints: Number,
                gradeScored: String}
        ]
    });
    Result = connection.model(collectionName, resultSchema);
}

function getData(cb) {
    Result.find({}, function (err, db_students) {
        if (err)
            console.log(err);
        cb(null, db_students);
    });
}


router.get('/xls/branchwise', function (req, res) {

    var college;
    if (typeof (req.query.college) !== 'undefined') {
        college = (req.query.college).toLowerCase();
    }
    else {
        college = '';
    }
    var degree = (req.query.degree).toLowerCase();
    var branch = (req.query.branch).toUpperCase();
    var batch = req.query.batch;
    var semester = req.query.semester;
    var collectionName = college + degree + branch + batch + 'sem' + semester;
    var fileName = global.mapBranchName[branch] + '.xls';

    var sess = req.session;
    sess.markListArray = [];
    var markListArray = sess.markListArray;

    function modelDataForBranch(markListArray, cb) {

        var requiredXLS = [];
        var subjectList = [];

        /*
         * This function fills the row with the grades scored by the student.
         **/
        function getDataForSubject(eachStudentForXLS, allSubject) {
            for (var ind = 0; ind < allSubject.length; ind++) {
                var eachSubject = allSubject[ind];
                eachStudentForXLS[eachSubject['subjectCode']] = eachSubject['gradeScored'];
                if (ind === allSubject.length - 1)
                    requiredXLS.push(eachStudentForXLS);
            }
        }

        /*
         * This function stores the Registration No., Name, Branch to the object which will
         * behave as a row for the excel sheet. It first initializes the row with some
         * default value ('X') and makes a call to getDataForSubject which fills the grade
         * for each subject which the student had appeared.
         */
        function generateRowForXls(subjectList) {
            for (var start = 0; start < markListArray.length; start++) {
                var eachStudentForXLS = {};
                eachStudentForXLS['Regd No'] = markListArray[start].registrationNo;
                eachStudentForXLS['Name'] = markListArray[start].name;
                //eachStudentForXLS['branch'] = markListArray[start].branch;
                for (var i = 0; i < subjectList.length; i++) {
                    eachStudentForXLS[subjectList[i]] = ' ';
                }
                var allSubject = markListArray[start].allsubject;
                getDataForSubject(eachStudentForXLS, allSubject);
                eachStudentForXLS['sgpa'] = markListArray[start].sgpa;
                if (start === markListArray.length - 1) {
                    return requiredXLS;
                }
            }
        }

        /*
         Get the list of subject by traversing the array and store it in a object.
         */
        var subjectExists = {};
        for (var m = 0; m < markListArray.length; m++) {
            markListArray[m] = markListArray[m].toObject();
            var eachSubject = markListArray[m].allsubject;
            for (var s = 0; s < eachSubject.length; s++) {
                if (subjectExists[eachSubject[s].subjectCode] !== true) {
                    subjectList.push(eachSubject[s].subjectCode);
                    subjectExists[eachSubject[s].subjectCode] = true;
                }
            }
            if (m === markListArray.length - 1) {
                return generateRowForXls(subjectList);
            }
        }
    }

    var markList = [];
    var branchName, year, whichSemester;
    //Here we need to get the data from mongoDB as per the selection made on user panel.
//    async.parallel([
    createSchema(collectionName);
    getData(function (err, db) {
        if (err)
            console.log(err);
        markListArray = db;
        var requiredXLS = [];
        requiredXLS = modelDataForBranch(markListArray);
        if (typeof requiredXLS === 'undefined' || requiredXLS.length === 0) {
            requiredXLS = [];
            var objectForXLS = {};
            objectForXLS['label'] = 'Some Error Occured! Please Try again later';
            requiredXLS.push(objectForXLS);
        }
        var transform = require('stream-transform');
        transform(requiredXLS,
            function (data) {
                return data;
            },
            function(err, output){
                res.end(output,'binary');
            });
    });

    /* 2011 1ST */
    //markListArray = require('/home/devil/Desktop/Project Data/NIST Batch 2011/1st Semester/JSON Format/COMPUTER SCIENCE & ENGINEERING.json');
    //var requiredXLS = [];
    //requiredXLS = modelDataForBranch(markListArray);
    //res.xls('INFORMATION TECHNOLOGY.xls', requiredXLS);
});


router.get('/xls/totalpassandfail', function (req, res) {
    var college, degree, branch, batch, semester;

    if (typeof (req.query.college) !== 'undefined') {
        college = (req.query.college).toLowerCase();
    }
    else {
        college = '';
    }

    if (typeof (req.query.degree) !== 'undefined') {
        degree = (req.query.degree).toLowerCase();
    }
    else {
        degree = '';
    }
    if (typeof (req.query.branch) !== 'undefined') {
        branch = (req.query.branch).toLowerCase();
    }
    else {
        branch = '';
    }
    if (typeof (req.query.batch) !== 'undefined') {
        batch = req.query.batch;
    }
    else {
        batch = '';
    }

    if (typeof (req.query.semester) !== 'undefined') {
        semester = req.query.semester;
        semester = 'sem' + semester;
    }
    else {
        semester = '';
    }

    var collectionName = college + degree + branch + batch + semester;
    var lastLabel = batch + ' batch ' + global.mapSemester[semester[semester.length - 1]] + ' Semester';
    createSchema(collectionName);
    getData(function (err, db) {
        if (err)
            console.log(err);
        markListArray = db;
        var requiredXLS = [];
        requiredXLS = driverForTotalPassAndFail.modelDataForPassAndFail(markListArray, lastLabel);
        if (typeof requiredXLS === 'undefined' || requiredXLS.length === 0) {
            requiredXLS = [];
            var objectForXLS = {};
            objectForXLS['label'] = 'Some Error Occured! Please Try again later';
            requiredXLS.push(objectForXLS);
        }
        res.xls('Total Pass & Fail.xls', requiredXLS);
    });
});

router.get('/xls/scoreanalysis', function (req, res) {
    var college, degree, branch, batch, semester;
    if (typeof (req.query.college) !== 'undefined') {
        college = (req.query.college).toLowerCase();
    }
    else {
        college = '';
    }

    if (typeof (req.query.degree) !== 'undefined') {
        degree = (req.query.degree).toLowerCase();
    }
    else {
        degree = '';
    }
    if (typeof (req.query.branch) !== 'undefined') {
        branch = (req.query.branch).toLowerCase();
    }
    else {
        branch = '';
    }
    if (typeof (req.query.batch) !== 'undefined') {
        batch = req.query.batch;
    }
    else {
        batch = '';
    }

    if (typeof (req.query.semester) !== 'undefined') {
        semester = req.query.semester;
        semester = 'sem' + semester;
    }
    else {
        semester = '';
    }

    var collectionName = college + degree + branch + batch + semester;
    var lastLabel = batch + ' batch ' + global.mapSemester[semester[semester.length - 1]] + ' Semester';
    createSchema(collectionName);
    getData(function (err, db) {
        if (err)
            console.log(err);
        markListArray = db;
        var requiredXLS = [];
        requiredXLS = driverForScoreAnalysis.modelDataForScoreAnalysis(markListArray, lastLabel);
        if (typeof requiredXLS === 'undefined' || requiredXLS.length === 0) {
            requiredXLS = [];
            var objectForXLS = {};
            objectForXLS['label'] = 'Some Error Occured! Please Try again later';
            requiredXLS.push(objectForXLS);
        }
        res.xls('Score Analysis.xls', requiredXLS);
    });
});


router.get('/xls/subjectwise', function (req, res) {
    var college, degree, branch, batch, semester;

    if (typeof (req.query.college) !== 'undefined') {
        college = (req.query.college).toLowerCase();
    }
    else {
        college = '';
    }

    if (typeof (req.query.degree) !== 'undefined') {
        degree = (req.query.degree).toLowerCase();
    }
    else {
        degree = '';
    }
    if (typeof (req.query.branch) !== 'undefined') {
        branch = (req.query.branch).toLowerCase();
    }
    else {
        branch = '';
    }
    if (typeof (req.query.batch) !== 'undefined') {
        batch = req.query.batch;
    }
    else {
        batch = '';
    }

    if (typeof (req.query.semester) !== 'undefined') {
        semester = req.query.semester;
        semester = 'sem' + semester;
    }
    else {
        semester = '';
    }

    var collectionName = college + degree + branch + batch + semester;
    createSchema(collectionName);
    getData(function (err, db) {
        if (err)
            console.log(err);
        markListArray = db;
        var requiredXLS = [];
        requiredXLS = driverForSubjectWise.modelDataForSubjectWise(markListArray);
        if (typeof requiredXLS === 'undefined' || requiredXLS.length === 0) {
            requiredXLS = [];
            var objectForXLS = {};
            objectForXLS['label'] = 'Some Error Occured! Please Try again later';
            requiredXLS.push(objectForXLS);
        }
        res.xls('Subject Wise.xls', requiredXLS);
    });
});

router.get('/xls/passfailpercentage', function (req, res) {
    var college, degree, branch, batch, semester;
    if (typeof (req.query.college) !== 'undefined') {
        college = (req.query.college).toLowerCase();
    }
    else {
        college = '';
    }

    if (typeof (req.query.degree) !== 'undefined') {
        degree = (req.query.degree).toLowerCase();
    }
    else {
        degree = '';
    }
    if (typeof (req.query.branch) !== 'undefined') {
        branch = (req.query.branch).toLowerCase();
    }
    else {
        branch = '';
    }
    if (typeof (req.query.batch) !== 'undefined') {
        batch = req.query.batch;
    }
    else {
        batch = '';
    }

    if (typeof (req.query.semester) !== 'undefined') {
        semester = req.query.semester;
        semester = 'sem' + semester;
    }
    else {
        semester = '';
    }

    var collectionName = college + degree + branch + batch + semester;
    createSchema(collectionName);
    getData(function (err, db) {
        if (err)
            console.log(err);
        markListArray = db;
        var requiredXLS = [];
        requiredXLS = driverForPassFail.modelDataForSubjectWise(markListArray);
        if (typeof requiredXLS === 'undefined' || requiredXLS.length === 0) {
            requiredXLS = [];
            var objectForXLS = {};
            objectForXLS['label'] = 'Some Error Occured! Please Try again later';
            requiredXLS.push(objectForXLS);
        }
        res.xls('Pass Fail.xls', requiredXLS);
    });
});


router.get('/xls/percentageanalysis', function (req, res) {
    var college, degree, branch, batch, semester;

    if (typeof (req.query.college) !== 'undefined') {
        college = (req.query.college).toLowerCase();
    }
    else {
        college = '';
    }

    if (typeof (req.query.degree) !== 'undefined') {
        degree = (req.query.degree).toLowerCase();
    }
    else {
        degree = '';
    }
    if (typeof (req.query.branch) !== 'undefined') {
        branch = (req.query.branch).toLowerCase();
    }
    else {
        branch = '';
    }
    if (typeof (req.query.batch) !== 'undefined') {
        batch = req.query.batch;
    }
    else {
        batch = '';
    }

    if (typeof (req.query.semester) !== 'undefined') {
        semester = req.query.semester;
        semester = 'sem' + semester;
    }
    else {
        semester = '';
    }

    var collectionName = college + degree + branch + batch + semester;
    createSchema(collectionName);
    getData(function (err, db) {
        if (err)
            console.log(err);
        markListArray = db;
        var requiredXLS = [];
        requiredXLS = driverForPercentageAnalysis.modelDataForPercentageAnalysis(markListArray);
        if (typeof requiredXLS === 'undefined' || requiredXLS.length === 0) {
            requiredXLS = [];
            var objectForXLS = {};
            objectForXLS['label'] = 'Some Error Occured! Please Try again later';
            requiredXLS.push(objectForXLS);
        }
        res.xls('Percentage Analysis.xls', requiredXLS);
    });
});


module.exports = router;

